VERSION=v3.0.0

gomod:
	go get chainmaker.org/chainmaker/chainconf/v3@$(VERSION)
	go get chainmaker.org/chainmaker/common/v3@$(VERSION)
	go get chainmaker.org/chainmaker/localconf/v3@$(VERSION)
	go get chainmaker.org/chainmaker/logger/v3@$(VERSION)
	go get chainmaker.org/chainmaker/pb-go/v3@$(VERSION)
	go get chainmaker.org/chainmaker/protocol/v3@$(VERSION)
	go get chainmaker.org/chainmaker/store/v3@$(VERSION)
	go get chainmaker.org/chainmaker/utils/v3@$(VERSION)
	go get chainmaker.org/chainmaker/vm/v3@$(VERSION)
	go mod tidy
	cat go.mod|grep chainmaker
