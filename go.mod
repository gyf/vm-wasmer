module chainmaker.org/chainmaker/vm-wasmer/v3

go 1.16

require (
	chainmaker.org/chainmaker/chainconf/v3 v3.0.0
	chainmaker.org/chainmaker/common/v3 v3.0.0
	chainmaker.org/chainmaker/logger/v3 v3.0.0
	chainmaker.org/chainmaker/pb-go/v3 v3.0.0
	chainmaker.org/chainmaker/protocol/v3 v3.0.0
	chainmaker.org/chainmaker/store/v3 v3.0.0
	chainmaker.org/chainmaker/utils/v3 v3.0.0
	chainmaker.org/chainmaker/vm/v3 v3.0.0
	github.com/stretchr/testify v1.8.0
)
