module chainmaker.org/chainmaker/vm-wasmer-test/v3

go 1.16

require (
	chainmaker.org/chainmaker/chainconf/v3 v3.0.0
	chainmaker.org/chainmaker/common/v3 v3.0.0
	chainmaker.org/chainmaker/localconf/v3 v3.0.0
	chainmaker.org/chainmaker/logger/v3 v3.0.0
	chainmaker.org/chainmaker/pb-go/v3 v3.0.0
	chainmaker.org/chainmaker/protocol/v3 v3.0.0
	chainmaker.org/chainmaker/store/v3 v3.0.0
	chainmaker.org/chainmaker/utils/v3 v3.0.0
	chainmaker.org/chainmaker/vm-wasmer/v3 v3.0.0-00010101000000-000000000000
	chainmaker.org/chainmaker/vm/v3 v3.0.0
	github.com/gogo/protobuf v1.3.2
	github.com/golang/mock v1.6.0
	github.com/mitchellh/mapstructure v1.5.0
	github.com/mr-tron/base58 v1.2.0
	github.com/spf13/cobra v1.1.1
	github.com/stretchr/testify v1.8.0
)

replace (
	chainmaker.org/chainmaker/vm-wasmer/v3 => ../
	github.com/RedisBloom/redisbloom-go => chainmaker.org/third_party/redisbloom-go v1.0.0
)
