/*
Copyright (C) BABEC. All rights reserved.
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package functional_verify

const (
	ConfigFilePath   = "../config/wx-org1-solo/chainmaker.yml"
	UserCertFilePath = "../config/wx-org1-solo/certs/user/client1/client1.sign.crt"
)
